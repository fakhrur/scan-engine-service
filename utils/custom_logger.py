class Logger():
    def __init__(self):
        self.color = {
            "bold": '\033[1m',
            "success": '\033[92m',
            "info": '\033[93m',
            "error": '\033[91m',
            "endc": '\033[0m'
        }

    def info(self, text, end="\n", symbol=None):
        if symbol is None:
            symbol = "[!] "
        print(self.color["info"] + symbol + text + self.color["endc"], end=end, flush=True)

    def error(self, text, end="\n", symbol=None):
        if symbol is None:
            symbol = "[!] "
        print(self.color["error"] + symbol + text + self.color["endc"], end=end, flush=True)

    def success(self, text, end="\n", symbol=None):
        if symbol is None:
            symbol = "[+] "
        print(self.color["success"] + symbol + text + self.color["endc"], end=end, flush=True)

    def important(self, text, end="\n", symbol=None):
        if symbol is None:
            symbol = "[+] "
        print(self.color["bold"] + symbol + text + self.color["endc"], end=end, flush=True)
