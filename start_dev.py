import re
import time
import platform
import subprocess
from watchdog.observers import Observer
from watchdog.events import PatternMatchingEventHandler
from utils.custom_logger import Logger

log = Logger()
os_platform = platform.system()
pods_name = []


def is_helm_installed():
    log.info("Check if helm installed...", end='')
    try:
        subprocess.check_output(['helm', 'version'])
        log.success("Done!", symbol='')
        return True
    except BaseException:
        log.error("Failed!", symbol='')
        log.error("Helm is not installed. Please install Helm first!")
        exit(0)


def start_minikube():
    log.info("Starting minikube and ingress...", end='')
    try:
        subprocess.check_output(["minikube", "start"], stderr=subprocess.DEVNULL)
        subprocess.check_output(["minikube", "addons", "enable", "ingress"], stderr=subprocess.DEVNULL)
        log.success("Done!", symbol='')
    except BaseException:
        log.error('Failed!', symbol='')


def add_airflow_repo():
    log.info('Adding airflow helm chart repo...', end='')
    repo_list = subprocess.check_output(['helm', 'repo', 'list']).decode('utf-8')
    already_added = re.findall(r'(bitnami\shttps://charts.bitnami.com/bitnami)', repo_list)
    if not already_added:
        subprocess.check_output(["helm", "repo", "add", "bitnami", "https://charts.bitnami.com/bitnami"])
    log.success("Done!", symbol='')


def run_airflow():
    log.info("Installing Airflow chart...", end='')
    subprocess.check_output(["helm", "install", "scan-engine", "-f", "values.yaml",
                             "bitnami/airflow"], stderr=subprocess.DEVNULL)
    log.success("Done!", symbol='')


def uninstall_airflow_chart():
    log.info("Uninstalling Airflow chart...", end='')
    subprocess.check_output(["helm", "uninstall", "scan-engine"], stderr=subprocess.DEVNULL)
    log.success("Done!", symbol='')


def get_pods_name():
    global pods_name
    pods_name = []
    pods = subprocess.check_output(["kubectl", "get", "pods"]).decode('utf-8').strip().split('\n')[1:]
    for i in pods:
        pods_name.append(i.split()[0])


def wait_pods_running():
    ready = False
    while not ready:
        pods = subprocess.check_output(["kubectl", "get", "pods"]).decode('utf-8').strip()
        if len(pods.split("\n")) == 6 and len(re.findall(("1/1"), pods)) == 5:
            ready = True


def copy_file_to_pod(path, dest=None):
    if not dest:
        dest = path

    for i in pods_name:
        if "airflow" not in i:
            continue

        subprocess.check_output(["kubectl", "cp", path, f"{i}:/opt/bitnami/airflow/{dest}"], stderr=subprocess.DEVNULL)


def delete_file_from_pod(path):
    for i in pods_name:
        if "airflow" not in i:
            continue

        subprocess.check_output(["kubectl", "exec", i, "--", "rm", "-rf",
                                 f"/opt/bitnami/airflow/{path}"], stderr=subprocess.DEVNULL)


def create_dir_in_pod(path):
    for i in pods_name:
        if "airflow" not in i:
            continue

        subprocess.check_output(["kubectl", "exec", i, "--", "mkdir",
                                 f"/opt/bitnami/airflow/{path}"], stderr=subprocess.DEVNULL)


def create_role_for_k8s():
    check = subprocess.check_output(['kubectl',
                                     'auth',
                                     'can-i',
                                     'get',
                                     'pods/log',
                                     '-n',
                                     'default',
                                     '--as=system:serviceaccount:default:default'],
                                    stderr=subprocess.DEVNULL).decode("utf-8").strip()
    if "yes" not in check:
        cmd_create_clusterrole = ['kubectl', 'create', 'clusterrole', 'pod-manager',
                                  '--verb=create,get,list,watch,delete', '--resource=pods,pods/log']
        cmd_clusterrolebinding = [
            'kubectl',
            'create',
            'clusterrolebinding',
            'pod-manager-clusterrolebinding',
            '--clusterrole=pod-manager',
            '--serviceaccount=default:default']
        subprocess.check_output(cmd_create_clusterrole, stderr=subprocess.DEVNULL)
        subprocess.check_output(cmd_clusterrolebinding, stderr=subprocess.DEVNULL)


def on_created(event):
    get_pods_name()
    if event.is_directory:
        log.info(f"Directory {event.src_path} has been created!")
        create_dir_in_pod(event.src_path)
        return

    log.info(f"File {event.src_path} has been created!")


def on_deleted(event):
    get_pods_name()
    log.info(f"File/Directory {event.src_path} has been deleted!")
    delete_file_from_pod(event.src_path)


def on_modified(event):
    if event.is_directory:
        return
    get_pods_name()
    log.info(f"File {event.src_path} has been modified")
    copy_file_to_pod(event.src_path)


def on_moved(event):
    get_pods_name()
    log.info(f"File {event.src_path} has been moved to {event.dest_path}")
    delete_file_from_pod(event.src_path)
    copy_file_to_pod(event.dest_path)


if __name__ == "__main__":
    log.info("Detected Platform: ", end="")
    log.important(os_platform, symbol="")

    start_minikube()
    create_role_for_k8s()

    is_helm_installed()
    add_airflow_repo()
    run_airflow()

    log.info("Add the following string into /etc/hosts")
    minikube_ip = subprocess.check_output(['minikube', 'ip']).decode('utf-8').strip()
    log.important(f"{minikube_ip} airflow.local\n", symbol='')

    log.info("Waiting all pods to be running. This might take a while!")
    log.info("If you think its too long, please check the pods status by running command: ", end="")
    log.important("kubectl get pods", symbol='')

    wait_pods_running()

    log.info("All pods already running, you can access the airflow from browser on ", end='')
    log.important("http://airflow.local\n", symbol='')
    log.info("=" * 50, symbol='')
    log.important("Starting the watcher for DAG changes", symbol='')
    log.info("=" * 50, symbol='')

    get_pods_name()
    copy_file_to_pod("./dags", "./")

    patterns = ["*"]
    ignore_patterns = None
    ignore_directories = False
    case_sensitive = True
    my_event_handler = PatternMatchingEventHandler(patterns, ignore_patterns, ignore_directories, case_sensitive)
    my_event_handler.on_created = on_created
    my_event_handler.on_deleted = on_deleted
    my_event_handler.on_modified = on_modified
    my_event_handler.on_moved = on_moved
    path = "./dags"
    go_recursively = True
    my_observer = Observer()
    my_observer.schedule(my_event_handler, path, recursive=go_recursively)
    my_observer.start()
    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        my_observer.stop()
        my_observer.join()
        uninstall_airflow_chart()
