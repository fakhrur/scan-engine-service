from datetime import datetime, timedelta
from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from airflow.operators.dummy_operator import DummyOperator
from airflow.providers.cncf.kubernetes.operators.kubernetes_pod import KubernetesPodOperator

default_args = {
    'owner': 'airflow',
    'description': 'Use of the KubernetesPodOperator',
    'depend_on_past': False,
    'start_date': datetime(2021, 5, 1),
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=5)
}

with DAG('example_k8s_pod_operator', default_args=default_args, schedule_interval=None, catchup=False) as dag:
    start_dag = DummyOperator(
        task_id='start_dag'
    )

    end_dag = DummyOperator(
        task_id='end_dag'
    )

    t1 = BashOperator(
        task_id='print_current_date',
        bash_command='date'
    )

    t2 = KubernetesPodOperator(
        task_id='docker_command_sleeps',
        name="command_sleeps",
        namespace="default",
        image='ubuntu:focal',
        cmds=["/bin/sleep", "10"],
    )

    t3 = KubernetesPodOperator(
        task_id='docker_command_hello',
        image='ubuntu:focal',
        namespace="default",
        name="command_hello",
        cmds=["echo", "{{ dag_run.conf['message'] }}"],
    )

    t4 = BashOperator(
        task_id='print_hello',
        bash_command='echo "hello world"'
    )

    start_dag >> t1

    t1 >> t2 >> t4
    t1 >> t3 >> t4

    t4 >> end_dag
